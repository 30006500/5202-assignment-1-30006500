/*  Campbell McChesney
    ID: 30006500  */
//research conducted by viewing sample code from https://www.youtube.com/watch?v=oDaTqKgPyGM to develop nested loops and array construction

//defnitions below creates matrices to be populated
var matrix1 = []
var matrix2 = []
var matrix3 = []

function matrixbuild(_matrix) //encapsulates the function that will generate the matrices
    {        
        for (i=0; i <= 2; i++) //this for loop iterates 3 times to build outer array 
           {  
                _matrix[i] = new Array(3) //a new inner array is created
                for (j=0; j <= 2; j++) // loops 3 times to build inner array
                {
                  _matrix[i][j] = Number(Math.floor(Math.random() * 21) - 10) //generates random number for the coordinates
                }
            }
    }

function matrixadd(_matrix1, _matrix2, _matrix3)
    { //research conducted by viewing sample code from https://www.youtube.com/watch?v=oDaTqKgPyGM to develop nested loops and array construction
        for (i=0; i <= 2; i++) //this for loop iterates 3 times to build outer array 
           {  
            _matrix3[i] = new Array(3) //a new inner array is created
                for (j=0; j <= 2; j++) // loops 3 times to build inner array
                {
                  _matrix3[i][j] = _matrix1[i][j] + _matrix2[i][j] //this adds matrix 1 and 2 and puts the result into matrix 3 in the current coordinates
                }
            }
     }

function matrixdisplay(_matrix)
    {
        for (i=0; i <= 2; i++) //this for loop iterates 3 times to build outer array 
            {  
            console.log(_matrix[i]) //this displays each row of the matrix to show a 3x3 grid
            }
    }
matrixbuild(matrix1) //this calls the matrixbuild function for matrix1
console.log("Matrix 1")
matrixdisplay(matrix1) //this calls the function that displays the matrix and passes the variable matrix1 into it
matrixbuild(matrix2) //this calls the matrixbuild function for matrix2
console.log("Matrix 2")
matrixdisplay(matrix2) //this call the function that displays the matrix and passes the vairable martix2 into it
matrixadd(matrix1, matrix2, matrix3)//this calls the matrixadd function and passes the 1st and 2nd matrices to it
console.log("Matrix 3")
matrixdisplay(matrix3) // this calls the function that displays the matrix and passes the variable matrix3 into it



