/*  Campbell McChesney
    id: 30006500
    Comp5202 - Assignment 1 */

function program() //This part of the program was encased in a function to easily run the program from the html button. Otherwise a function would not be needed.
    {
    var Username = document.getElementById("Username").value //Gets the value entered into the feild "Username" in the HTML page and assigns it to a variable
    var Password1 = document.getElementById("Password").value //Gets the value entered into the feild "Password" in the HTML page and assigns it to a variable
    var Password2 = document.getElementById("Password2").value//Gets the value entered into the feild "Password2" in the HTML page and assigns it to a variable

    usercheck(Username) //Calls the function and passes the variable Username (created above) into the function for condition checking
    passcheck(Password1, Password2) //Calls the function and passes the two variables Password and Password 2 (created above) into the function for condition checking
    
    }

function usercheck(_username) //Defines the function and sets the parameter to be used inside
    {
        if (_username.length < 8) //Condition check to see in the parameter length is less then 8 characters.  If it is then it returns the message below
            {
            document.getElementById("userout").innerHTML = ("Username must be at least 8 characters long")
            console.log('Username must be at least 8 characters long')
            }
        else //If the parameter length is 8 character or more, then it returns the message below
            {
            document.getElementById("userout").innerHTML = ("Username accepted")
            console.log(`Username accepted`)
            }
    }

function passcheck(_password1, _password2) //Defines function and sets the parameters to be used inside
    {
        if (_password1.length > 7) //Condition check to see if the password is 8 characters or longer. If it is then the next condition check is run
            {
                if (_password1 === _password2) //Checks the two password entires are identical.  If they are then the message below is returned
                    {
                    document.getElementById("passout").innerHTML = ("Password Set")
                    console.log(`Password set`)
                    }
                else //If the passwords are not identical then the message below is returned
                    {
                    document.getElementById("passout").innerHTML = ("Passwords do not match, please enter them again")
                    console.log(`Passwords do not match, please enter them again`)
                    }
            }

        else 
            {
                document.getElementById("passout").innerHTML = ("Password must be at least 8 characters long") //If the password is less than 8 characters long then this message will be displayed
                console.log("Password must be at least 8 characters long")
            }
    }