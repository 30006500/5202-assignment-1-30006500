/*  Campbell McChesney
    id: 30006500
    Comp5202 - Assignment 1 */

    function program()
        {
            var Name = document.getElementById("Name").value //Gets Employee Name from html feild and assigns it to variable "Name"
            var Salary = Number(document.getElementById("Salary").value)//Gets Employee Salary from html field and assigns it to variable "Salary"
            var Taxrate = Number(document.getElementById("Taxrate").value)// Gets Employee Tax Rate and assigns it to variable "Taxrate"
            var Tax = 0 //Creates a number variable called Tax with an initial value of 0
                  
            let Employee1 = new Employee(Name, Salary, Taxrate) //creates a new instance of the class "Employee" called "Employee1" using the values supplied"
           

            Tax = (Employee1.eSalary / 100) * Employee1.eTaxrate //Calcluates the Employees Tax dollar value using getters from the class
            
            
            document.getElementById("output").innerHTML =  `Employee Financial Details<br><br> 
                                                            Name: ${Employee1.eName}<br>
                                                            Salary: $${Employee1.eSalary}<br>
                                                            Tax Rate: ${Employee1.eTaxrate}%<br>
                                                            Tax Paid: $${Tax}<br>
                                                            Net Pay: $${netsalary(Employee1.eSalary, Tax)}` //Displays the processed information using getter and a function which has been passed getter values and the variable Tax
        
        console.log(`Employee Financial Details<br><br> 
        Name: ${Employee1.eName}<br>
        Salary: $${Employee1.eSalary}<br>
        Tax Rate: ${Employee1.eTaxrate}%<br>
        Tax Paid: $${Tax}<br>
        Net Pay: $${netsalary(Employee1.eSalary, Tax)}`)                                                
        }

    class Employee //Creates a class called Employee
        {
            constructor(Name, Salary, Taxrate) //defines the constructor and defines the parameters from the vabribles supplied at the start of the program
                {
                    this.eName = Name 
                    this.eSalary = Salary
                    this.eTaxrate = Taxrate //These three lines set the parameter names to the variable names entered into the constructor brackets
                }

            get eName() 
                {
                    return this._Name
                }

            set eName(value)
                {
                    this._Name = value
                }

            get eSalary()
                {
                    return this._Salary
                }

            set eSalary(value)
                {
                    this._Salary = value
                }

            get eTaxrate()
                {
                    return this._Taxrate
                }
            
            set eTaxrate(value)
                {
                    this._Taxrate = value
                }
        }

function netsalary(_Salary, _Tax) //This function carries out the calculation of the Net Pay.  It parameters are set when the function is called
            {
                Net = _Salary - _Tax
                return Net //This returns the result of the Variable Net when then function is run.
            }
